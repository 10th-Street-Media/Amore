# Amore

The desire to make a dating platform for the Fediverse led to my creating Amore. Like most dating websites, it will allow users to create extensive personal profiles, but it will also bring the power of the Fediverse to allow them to connect with a wider range of potential partners. They won't face the limited selection of people on one dating website, but can interact with people on other websites, as well.
